---
title: "为 Gitlab 文档翻译贡献一份力量"
subtitle: "了解如何协wendangwendang同翻译"
tags: ["文档","翻译"]
---

## 前言
Gitlab 文档的翻译是一份艰巨而又充满挑战的工作，目前的中文文档站 https://docs.gitlab.com.cn 仅有极少部分的翻译，